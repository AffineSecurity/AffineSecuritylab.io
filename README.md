### AffineSecurity

Welcome to my blog's source code. You'll find all the relevant files that I've created specifically for this blog wihin the repo. If you're looking for inspiration for your own blog, I recommed you check out the themes/hello-friend-ng folder and the static folder as they both have been modified to some degree.

You'll also see another beatuful readme within the hello-friend-ng folder. This is because I'm using the theme of the same name by the wonderful Djordje Atlialp. Be sure to check out the main project on it's [GitHub page](https://github.com/rhazdon/hugo-theme-hello-friend-ng).