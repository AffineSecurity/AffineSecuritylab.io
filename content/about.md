---
title: "Who is AffineSecurity?"
date: 2020-09-08T18:33:46-04:00
image: /images/about/headshot.png
draft: false 
---
My name is Sam. I'm a security professional who specializes in the field of offensive security. My current work focuses on incident response, digital forensics, and purple teaming. 

I got my start in IT in college when I competed in the National Cyber League Fall 2018 competition. I switched majors to Computer Technology in Spring 2019 where I learned about system administration and network administration. During my time in college, I completed internships where I focused on security architecture and software/container security. Since then, I've develed deeply into the worlds of both offensive and defensive security. I'm now working to combine my passion for both through purple teaming. 
