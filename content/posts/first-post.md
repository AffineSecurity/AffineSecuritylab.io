---
title: "Let's Get This Show on the Road"
subtitle: It's finally here
date: 2020-09-12T14:06:05-04:00
toc: false 
categories: 
  - General 
draft: false 
---
Well this certainly took a lot longer than I thought it would...

I've been thinking about making a blog since earlier this year, but if I'm being honest, I didn't really have a good reason to make one aside from it being something I should have (which in my opinion is not a very good reason). So why now? What's changed?

The catalyst for this blog really came from TryHackMe - a Hack the Box-like platform that focuses on teaching and developing pentesting skills through various challenges. For each THM room (includes the box + questions associated with it), there is usually a section that has links to user submitted guides on how to complete a specific room. After I had done a few rooms, I realized that there were some that didn't have any of these write-ups associated with them. I saw this as a great opportunity to start getting more involved in the security community, and thus I found my reason for this blog.

So this begs the question - is my blog exclusively going to be THM write-ups? The answer - no. While it's a good starting point, I don't want to confine myself to one type of content. Also, writing a good quality write-up is not something I can do quickly or consistently, and I would like to have at least one or two pieces of content I can put out quite regularly. I've already found or thought of a couple ideas that I think will be both interesting for me to research and useful and interesting for others to read. 

So in summary, here's a very quick tl;dr of what you can expect from this blog:
- Security or IT-focused content about 90%-95% of the time
- Regularly scheduled content alongside ad-hoc posts
- Content that (in my opinion) is interesting for me to write and for others to read

For those of you who made it this far - thank you, it means a lot! I hope to see you back when my first "real" post goes up.
