---
title: "Efficient Scanning with masscan and nmap"
subtitle: Those minutes add up...
date: 2020-09-27T17:56:59-04:00
toc: false
categories: 
  - Tutorials
  - Tools
draft: false 
---
## Introduction
I've got to admit - I'm a bit obsessed with efficiency. If there is a tool or method that can help me complete a task quicker, I'm almost always a fan of it. So naturally, one of my least favorite parts of doing CTFs is scanning. I always felt that nmap took forever to scan the hosts I was working with. However, I came across a tool today that, when combined with nmap, can result in scan times that are much quicker than running nmap on its own. 


## Meet masscan
The basic masscan command will be using in this scenario goes as follows: 

```bash
sudo masscan --rate=1000 -p 1-65535 10.10.112.117 -e tun0 -oL masscan.txt
```

The above command tells masscan to scan the above port ranges on the IP address 10.10.112.117 at a rate of 1000 packets/second. It also specifies to use the tun0 interface (this is what I use for TryHackMe boxes) and outputs the scan results to a file called `masscan.txt`. So why exactly is this useful aside from being fast? The output format masscan uses is incredibly easy to modify with `cut` and `awk`. Take a look.

```console
#masscan
open tcp 8083 10.10.112.117 1601244923
open tcp 42670 10.10.112.117 1601244946
open tcp 3873 10.10.112.117 1601244947
open tcp 4445 10.10.112.117 1601244951
open tcp 4446 10.10.112.117 1601244954
open tcp 4713 10.10.112.117 1601244957
open tcp 22 10.10.112.117 1601244958
open tcp 1099 10.10.112.117 1601244973
open tcp 4712 10.10.112.117 1601244974
open tcp 1090 10.10.112.117 1601244986
open tcp 3306 10.10.112.117 1601244988
open tcp 111 10.10.112.117 1601244992
open tcp 8009 10.10.112.117 1601244992
open tcp 1098 10.10.112.117 1601244996
open tcp 8080 10.10.112.117 1601245005
open tcp 80 10.10.112.117 1601245006
open tcp 50934 10.10.112.117 1601245020
open tcp 4457 10.10.112.117 1601245031
open tcp 4444 10.10.112.117 1601245034
open tcp 42291 10.10.112.117 1601245039
# end
```

Let's see what we can do with this...


## Command line magic
To get the output from the masscan file into a more useful format, we're going to run the following command:

```bash
cut -d " " -f 3 -s masscan.txt | awk -v RS='' '{gsub("\n", ","); print}'
```

So what exactly are all these commands doing? Firstly, our two `cut` statement use spaces as delimeters to cut the file down to only show the port numbers within the file. The `awk` statement then replaces all of the new line markers (`\n`) with commas, turning this into a csv-like format.

```console
8083,42670,3873,4445,4446,4713,22,1099,4712,1090,3306,111,8009,1098,8080,80,50934,4457,4444,42291
```

If you really wanted to, you could also toss in a `sort -n` between the `cut` and `awk` statements to get these guys in order, but as far as I'm aware that doesn't really serve a functional purpose for what we're going to do. Speaking of which...


## Throwing it all in nmap
For those who've used nmap quite a bit, you probably already know where this is going. For those who aren't as familiar, let me explain. Nmap can take a range of ports as an argument for its `-p` option, but only if the list is comma-separated with no spaces. So now that we've figured out how to do that, we can input our previous command into an nmap command and scan the ports we already know are open.

```bash
sudo nmap -A -p $(cut -d " " -f 3 -s masscan.txt | awk -v RS='' '{gsub("\n", ","); print}') 10.10.112.117
```

From here, nmap will run its scan as per usual with the added efficiency of only scanning the ports we know are open. When I tested this process against the same nmap scan done against all ports, I found that I cut the time down by about half. 

To further improve the process, I've built a quick shell script that automates this process a little more. If you'd like to use it, you can grab it from [my GitLab repo](https://gitlab.com/AffineSecurity/massmap). It's pretty barebones as of now, but I'll try to see if I can't make some quality of life improvements or add in some functionality that makes sense.


## Conclusion
Scanning is slow, but it doesn't have to be. With masscan and nmap, you're able to take advantage of the benefits each program provides to more efficiently scan hosts. So if you're impatient and want to be efficient like me,  you can use this method for running your network scans just a little bit faster.

